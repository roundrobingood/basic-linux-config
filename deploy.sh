#!/bin/bash
starttime=`date +%s`
ansible-playbook -i hosts deploy.yml
finishtime=`date +%s`
printf 'Seconds:'
echo $finishtime-$starttime | bc

